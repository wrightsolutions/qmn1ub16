# Commands and services configuration as preferred for our setup

nagios127localhost_hostservice:
  file.managed:
  - name: /etc/nagios3/conf.d/localhost_nagios2.cfg
  - contents: |
      # A simple configuration file for monitoring the local host
      # This can serve as an example for configuring other servers;
      # Custom services specific to this host are added here, but services
      # defined in nagios2-common_services.cfg may also apply.
      # 
      
      define host{
              use                     generic-host            ; Name of host template to use
              host_name               localhost
              alias                   mn127
              address                 127.0.0.1
              }
      
      # Define a service to check the disk space of the root partition
      # on the local machine.  Warning if < 20% free, critical if
      # < 10% free space on partition.
      # For more targetted checks see disk.cfg (a 2 line extract included below)
      # command_name	check_disk
      # command_line	/usr/lib/nagios/plugins/check_disk -w '$ARG1$' -c '$ARG2$' -e -p '$ARG3$'
      
      #define service{
      #        use                             generic-service         ; Name of service template to use
      #        host_name                       localhost
      #        service_description             Disk Space
      #        check_command                   check_all_disks!20%!10%
      #        }
      
      
      
      # Define a service to check the number of currently logged in
      # users on the local machine.  Warning if > 20 users, critical
      # if > 50 users.
      
      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             Current Users
              check_command                   check_users!2!4
              }
      
      
      # Define a service to check the number of currently running procs
      # on the local machine. See check_command below where first arg is
      # Warning and second arg is Critical num.
      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             Total Processes
      		check_command                   check_procs!130!150
              }
      
      
      
      # Define a service to check the load on the local machine. 
      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             Current Load
      		check_command                   check_load!5.0!4.0!3.0!10.0!6.0!4.0
              }

      # Disk space inode check of /var using warn at 92 and critical at 90
      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             Disk Space /var INODES
              check_command                   check_disk_inode!92%!90%!/var
              }

      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             DiskSpaceFSvar
              check_command                   check_disk!20%!10%!/var
              }

  - backup: minion


nagios127services:
  file.managed:
  - name: /etc/nagios3/conf.d/services_nagios2.cfg
  - contents: |
      # check that web services are running
      #define service {
      #        hostgroup_name                  http-servers
      #        service_description             HTTP
      # 	check_command                   check_http
      #        use                             generic-service
      #	notification_interval           0 ; set > 0 if you want to be renotified
      #}
      
      # check that ssh services are running
      define service {
              hostgroup_name                  ssh-servers
              service_description             SSH
      	check_command                   check_ssh
              use                             generic-service
      	notification_interval           0 ; set > 0 if you want to be renotified
      }
       
  - backup: minion


nagios127mn_localhost_script:
  file.managed:
  - name: /root/mn_nag_localhost.sh
  - contents: |
      #!/bin/sh
      SPREFIX_='/usr/bin/salt-call --local state.sls '
      ${SPREFIX_} mn_nagios_plugins_config_disk_inode
      [ $? != 0 ] && exit 101
      ${SPREFIX_} mn_nag_localhost_mn
      [ $? != 0 ] && exit 102
      ${SPREFIX_} mn_nag_localhost
      [ $? != 0 ] && exit 103
      exit 0
       
  - mode: 750
  - backup: minion

