# nagios-plugins/config - Inode check_disk variants
nagios_plugins_config_disk_inode:
  file.managed:
  - name: /etc/nagios-plugins/config/disk_inode.cfg
  - contents: |
      # Inode check_disk variants
      # 'check_disk_inode' command definition
      define command{
      	command_name	check_disk_inode
      	command_line	/usr/lib/nagios/plugins/check_disk -W '$ARG1$' -K '$ARG2$' -e -p '$ARG3$'
      	}
      
      # 'check_all_disk_inodes' command definition
      define command{
      	command_name	check_all_disk_inodes
      	command_line	/usr/lib/nagios/plugins/check_disk -W '$ARG1$' -K '$ARG2$' -e
      	}
      
      # 'ssh_disk_inode' command definition
      define command{
      	command_name	ssh_disk_inode
      	command_line    /usr/lib/nagios/plugins/check_by_ssh -H '$HOSTADDRESS$' -C "/usr/lib/nagios/plugins/check_disk -W '$ARG1$' -K '$ARG2$' -e -p '$ARG3$'"
      	}
      
      ####
      # use these checks, if you want to test IPv4 connectivity on IPv6 enabled systems
      ####
      
      # 'ssh_disk_inode_4' command definition
      define command{
              command_name    ssh_disk_inode_4
              command_line    /usr/lib/nagios/plugins/check_by_ssh -H '$HOSTADDRESS$' -C "/usr/lib/nagios/plugins/check_disk -W '$ARG1$' -K '$ARG2$' -e -p '$ARG3$'" -4
              }

  - mode: 644
  - backup: minion

