# Nagios central, Nagios nrpe, sudo for nagios
nagios_central:
  # Install Nagios central server
  pkg.installed:
    - pkgs:
      - nagios3-core
      - nagios3-common
      - nagios3-cgi
      - nagios-images
      - nagios-nrpe-server
      - nagios-nrpe-plugin
      - nagios-plugins-common
      - nagios-plugins-basic
      - nagios-plugins-standard
      # - nagios-plugins
    - install_recommends: false


nagios_nrpe:
  # Install Nagios nrpe remote client
  pkg.installed:
    - pkgs:
      - nagios-nrpe-server
#     - monitoring-plugins
      - monitoring-plugins-basic
      - nagios-plugins-common
#     - nagios-plugins-basic
      - nagios-plugins-standard
#     - nagios-plugins
    - install_recommends: false


sudoers_monitoring_local:
  file.managed:
  - name: /etc/sudoers.d/90-monitoring_local
  - contents: |
      # Local Example: nagios    ALL=(ALL) NOPASSWD: /usr/lib/nagios/plugins/,/usr/bin/arrayprobe
      # On Debian the Nagios plugin scripts live in /usr/lib/nagios/plugins
      nagios    ALL=(ALL) NOPASSWD: /usr/lib/nagios/plugins/, /usr/bin/arrayprobe, /usr/lib64/nagios/plugins/check_*
      %users    ALL=(ALL) NOPASSWD: /usr/bin/uptime, /usr/bin/downtimes
  - order: last
  - backup: minion

