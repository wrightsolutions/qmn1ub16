# Reset a handful of configuration files to the installed default versions
# Alternative to: apt-get --reinstall -o Dpkg::Options::="--force-confask" install nagios3-common
#
# Also as the 'last' of the _reset style recipes, this one will write a couple of shell script helpers
# which can be manually run as required.

nagios127commands_default:
  file.managed:
  - name: /etc/nagios3/commands.cfg
  - contents: |
      ###############################################################################
      # COMMANDS.CFG - SAMPLE COMMAND DEFINITIONS FOR NAGIOS 
      ###############################################################################
      
      
      ################################################################################
      # NOTIFICATION COMMANDS
      ################################################################################
      
      
      # 'notify-host-by-email' command definition
      define command{
      	command_name	notify-host-by-email
      	command_line	/usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\nHost: $HOSTNAME$\nState: $HOSTSTATE$\nAddress: $HOSTADDRESS$\nInfo: $HOSTOUTPUT$\n\nDate/Time: $LONGDATETIME$\n" | /usr/bin/mail -s "** $NOTIFICATIONTYPE$ Host Alert: $HOSTNAME$ is $HOSTSTATE$ **" $CONTACTEMAIL$
      	}
      
      # 'notify-service-by-email' command definition
      define command{
      	command_name	notify-service-by-email
      	command_line	/usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\n\nService: $SERVICEDESC$\nHost: $HOSTALIAS$\nAddress: $HOSTADDRESS$\nState: $SERVICESTATE$\n\nDate/Time: $LONGDATETIME$\n\nAdditional Info:\n\n$SERVICEOUTPUT$\n" | /usr/bin/mail -s "** $NOTIFICATIONTYPE$ Service Alert: $HOSTALIAS$/$SERVICEDESC$ is $SERVICESTATE$ **" $CONTACTEMAIL$
      	}
      
      
      
      
      
      ################################################################################
      # HOST CHECK COMMANDS
      ################################################################################
      
      # On Debian, check-host-alive is being defined from within the
      # nagios-plugins-basic package
      
      ################################################################################
      # PERFORMANCE DATA COMMANDS
      ################################################################################
      
      
      # 'process-host-perfdata' command definition
      define command{
      	command_name	process-host-perfdata
      	command_line	/usr/bin/printf "%b" "$LASTHOSTCHECK$\t$HOSTNAME$\t$HOSTSTATE$\t$HOSTATTEMPT$\t$HOSTSTATETYPE$\t$HOSTEXECUTIONTIME$\t$HOSTOUTPUT$\t$HOSTPERFDATA$\n" >> /var/lib/nagios3/host-perfdata.out
      	}
      
      
      # 'process-service-perfdata' command definition
      define command{
      	command_name	process-service-perfdata
      	command_line	/usr/bin/printf "%b" "$LASTSERVICECHECK$\t$HOSTNAME$\t$SERVICEDESC$\t$SERVICESTATE$\t$SERVICEATTEMPT$\t$SERVICESTATETYPE$\t$SERVICEEXECUTIONTIME$\t$SERVICELATENCY$\t$SERVICEOUTPUT$\t$SERVICEPERFDATA$\n" >> /var/lib/nagios3/service-perfdata.out
      	}
      
      
  - backup: minion


nagios127hostgroups_default:
  file.managed:
  - name: /etc/nagios3/conf.d/hostgroups_nagios2.cfg
  - contents: |
      # Some generic hostgroup definitions
      
      # Simple wildcard hostgroup
      define hostgroup {
              hostgroup_name  all
      		alias           All Servers
      		members         *
              }
      
      # Hostgroup of Linux servers
      define hostgroup {
              hostgroup_name  ubuntu-servers
      		alias           Ubuntu Linux Servers
      		members         localhost
              }
      
      # Hostgroup of web servers
      define hostgroup {
              hostgroup_name  http-servers
      		alias           HTTP servers
      		members         localhost
              }
      
      # Hostgroup of ssh-accessible servers
      define hostgroup {
              hostgroup_name  ssh-servers
      		alias           SSH servers
      		members         localhost
              }
      
       
  - backup: minion
  - require:
    - file: nagios127commands_default


nagios127mn_localhost_reset_script:
  file.managed:
  - name: /root/mn_nag_localhost_reset.sh
  - contents: |
      #!/bin/sh
      SPREFIX_='/usr/bin/salt-call --local state.sls '
      ${SPREFIX_} mn_nag_localhost_services_timeperiods_reset
      [ $? != 0 ] && exit 101
      ${SPREFIX_} mn_nag_commands_cfg_reset
      [ $? != 0 ] && exit 102
      exit 0
       
  - mode: 750
  - backup: minion


nagios127mn_localhost_copyhelp_script:
  file.managed:
  - name: /root/mn_nag_copyhelp.sh
  - contents: |
      #!/bin/sh
      /bin/cp -ip /root/mn_nag*.sls /srv/salt/
      #SPREFIX_='/usr/bin/salt-call --local state.sls '
      printf "+++\n"
      printf "Configure localhost: /root/mn_nag_localhost.sh\n"
      printf "Reset localhost configs: /root/mn_nag_localhost_reset.sh\n"
      exit 0
       
  - mode: 750
  - backup: minion


nagios_icli3script:
  file.managed:
  - name: /root/icli3.sh
  - contents: |
      #!/bin/sh
      /usr/bin/icli --config /etc/nagios3/nagios.cfg -f /var/cache/nagios3/status.dat $*
      exit $?
      # /usr/sbin/nagios3 -v /etc/nagios3/nagios.cfg && systemctl reload nagios3

  - mode: 750
  - backup: minion


# - name: /usr/sbin/nagios3 -v /etc/nagios3/nagios.cfg && systemctl reload nagios3
# - order: last
# - require:
#   - file: nagios127commands_default

