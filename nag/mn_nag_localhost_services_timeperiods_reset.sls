# Within the conf.d directory reset the following 4 files to default contents
#  - name: /etc/nagios3/conf.d/generic-service_nagios2.cfg
#  - name: /etc/nagios3/conf.d/services_nagios2.cfg
#  - name: /etc/nagios3/conf.d/timeperiods_nagios2.cfg
#  - name: /etc/nagios3/conf.d/localhost_nagios2.cfg


nagios127generic_service_default:
  file.managed:
  - name: /etc/nagios3/conf.d/generic-service_nagios2.cfg
  - contents: |
      # generic service template definition
      define service{
              name                            generic-service ; The 'name' of this service template
              active_checks_enabled           1       ; Active service checks are enabled
              passive_checks_enabled          1       ; Passive service checks are enabled/accepted
              parallelize_check               1       ; Active service checks should be parallelized (disabling this can lead to major performance problems)
              obsess_over_service             1       ; We should obsess over this service (if necessary)
              check_freshness                 0       ; Default is to NOT check service 'freshness'
              notifications_enabled           1       ; Service notifications are enabled
              event_handler_enabled           1       ; Service event handler is enabled
              flap_detection_enabled          1       ; Flap detection is enabled
              failure_prediction_enabled      1       ; Failure prediction is enabled
              process_perf_data               1       ; Process performance data
              retain_status_information       1       ; Retain status information across program restarts
              retain_nonstatus_information    1       ; Retain non-status information across program restarts
      		notification_interval           0		; Only send notifications on status change by default.
      		is_volatile                     0
      		check_period                    24x7
      		normal_check_interval           5
      		retry_check_interval            1
      		max_check_attempts              4
      		notification_period             24x7
      		notification_options            w,u,c,r
      		contact_groups                  admins
              register                        0       ; DONT REGISTER THIS DEFINITION - ITS NOT A REAL SERVICE, JUST A TEMPLATE!
              }

  - backup: minion


nagios127services_default:
  file.managed:
  - name: /etc/nagios3/conf.d/services_nagios2.cfg
  - contents: |
      # check that web services are running
      define service {
              hostgroup_name                  http-servers
              service_description             HTTP
       	check_command                   check_http
              use                             generic-service
      	notification_interval           0 ; set > 0 if you want to be renotified
      }
      
      # check that ssh services are running
      define service {
              hostgroup_name                  ssh-servers
              service_description             SSH
      	check_command                   check_ssh
              use                             generic-service
      	notification_interval           0 ; set > 0 if you want to be renotified
      }
       
  - backup: minion


nagios127timeperiods_default:
  file.managed:
  - name: /etc/nagios3/conf.d/timeperiods_nagios2.cfg
  - contents: |
      ###############################################################################
      # timeperiods.cfg
      ###############################################################################
      
      # This defines a timeperiod where all times are valid for checks, 
      # notifications, etc.  The classic "24x7" support nightmare. :-)
      
      define timeperiod{
              timeperiod_name 24x7
              alias           24 Hours A Day, 7 Days A Week
              sunday          00:00-24:00
              monday          00:00-24:00
              tuesday         00:00-24:00
              wednesday       00:00-24:00
              thursday        00:00-24:00
              friday          00:00-24:00
              saturday        00:00-24:00
              }
      
      # Here is a slightly friendlier period during work hours
      define timeperiod{
              timeperiod_name workhours
              alias           Standard Work Hours
              monday          09:00-17:00
              tuesday         09:00-17:00
              wednesday       09:00-17:00
              thursday        09:00-17:00
              friday          09:00-17:00
              }
      
      # The complement of workhours
      define timeperiod{
              timeperiod_name nonworkhours
              alias           Non-Work Hours
              sunday          00:00-24:00
              monday          00:00-09:00,17:00-24:00
              tuesday         00:00-09:00,17:00-24:00
              wednesday       00:00-09:00,17:00-24:00
              thursday        00:00-09:00,17:00-24:00
              friday          00:00-09:00,17:00-24:00
              saturday        00:00-24:00
              }
      
      # This one is a favorite: never :)
      define timeperiod{
              timeperiod_name never
              alias           Never
              }
      
      # end of file
  - backup: minion


nagios127localhost_hostservice_default:
  file.managed:
  - name: /etc/nagios3/conf.d/localhost_nagios2.cfg
  - contents: |
      # A simple configuration file for monitoring the local host
      # This can serve as an example for configuring other servers;
      # Custom services specific to this host are added here, but services
      # defined in nagios2-common_services.cfg may also apply.
      # 
      
      define host{
              use                     generic-host            ; Name of host template to use
              host_name               localhost
              alias                   localhost
              address                 127.0.0.1
              }
      
      # Define a service to check the disk space of the root partition
      # on the local machine.  Warning if < 20% free, critical if
      # < 10% free space on partition.
      
      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             Disk Space
              check_command                   check_all_disks!20%!10%
              }
      
      
      
      # Define a service to check the number of currently logged in
      # users on the local machine.  Warning if > 20 users, critical
      # if > 50 users.
      
      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             Current Users
              check_command                   check_users!20!50
              }
      
      
      # Define a service to check the number of currently running procs
      # on the local machine.  Warning if > 250 processes, critical if
      # > 400 processes.
      
      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             Total Processes
      		check_command                   check_procs!250!400
              }
      
      
      
      # Define a service to check the load on the local machine. 
      
      define service{
              use                             generic-service         ; Name of service template to use
              host_name                       localhost
              service_description             Current Load
      		check_command                   check_load!5.0!4.0!3.0!10.0!6.0!4.0
              }

  - backup: minion

