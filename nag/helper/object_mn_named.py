#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

import argparse
from datetime import datetime as dt
#from os import getenv as osgetenv
from sys import argv,exit

""" Set 3 values in icinga/object config for a node based on args.
In particular the planned supported examples with 0,false equivalent and 1,true,yes equivalent
./object_mn_named.py --hostname server128 --alias s128 --address 192.168.0.128

Prints to stdout. Might want to redirect (>) to /etc/icinga/objects/mn_alias.cfg
"""

DQ=chr(34)

verbosity_global = 1
# Above is set by default to 1. Also see __name__ == '__main__' section later.

ADESC="""Arguments for setting values in a node file in /etc/icinga/objects/ """

# Below we have replaced ${distro_id} so treated as literal { rather than template place
OBJECTS_MN_TEMPLATE="""### hhhost ###
#		use				generic-host
###

define host{{
 use							generic-host            
 host_name						{0}
 alias							{1}
 address						{2}
}}

### ssservice ###
#		use				generic-service		; Name of service template to use
###

define service{{
 use                             generic-service         ; Name of service template to use
 host_name                       {0}
 service_description             Server load
 check_command                   rpe2load15105
 }}


define service{{
 use                             generic-service         ; Name of service template to use
 host_name                       {0}
 service_description             Process with arg datadir
 check_command                   rpe2procs1datadir
 }}

define service{{
 use                             generic-service         ; Name of service template to use
 host_name                       {0}
 service_description             Network tcp/udp connections
 check_command                   ncon2crit128one
 }}

#define service{{
# use                             generic-service         ; Name of service template to use
# host_name                       {0}
# service_description             Disk Space
# check_command                   check_nrpe_arg!check_disk_all
# }}

"""


def false_unless_true(bool_str):
	""" Process an arg into lowercase true or false 
	based on appropriate interpretation of the string.
	Although argument is labelled 'bool_str' we accept and
	deal with other cases typical of a non validated input
	In argparse might want type=false_unless_true (untested)
	"""
	bool_return = False
        # Do keep False as the default or trace conditions carefully before changing
	try:
		blower = bool_str.strip().lower()
	except:
		# Not a string
		if bool_str is True:
			bool_return = True
		else:
			# False or some other non-string value
			pass
			#print(bool_str,bool_str)
	try:
		blower_int = int(blower)
		if 1 == blower_int:
			bool_return = True
	except:
		if bool_return is False:
			if bool_str is False:
				pass
			else:
				try:
					if blower in ('true', 't', 'yes'):
						bool_return = True
				except:
					EMSG_PREFIX = 'Unexpected value supplied: '
					raise Exception(EMSG_PREFIX+bool_str)

	return bool_return


def false_unless_true_lowercase(bool_str):
	""" Wrapper around false_unless_true where we REALLY want
	a response that is a stringified lowercase true or false
	"""
	bool_tf = false_unless_true(bool_str)
	bool_lower = 'true'
	if bool_tf is False:
		bool_lower = 'false'
	return bool_lower


if __name__ == '__main__':

	program_binary = argv[0].strip()

	par = argparse.ArgumentParser(description=ADESC)
	# Avoid type=bool or similar as that kind of processing is after args assigned here.
	par.add_argument('--hostname', default='server128',
				action='store',
				dest='hname',
				help='host_name entry for the host')
	par.add_argument('--alias', default='s128',
				action='store',
				dest='halias',
				help='alias entry for the host')
	par.add_argument('--address', default='192.168.0.128',
				action='store',
				dest='haddress',
				help='ip address entry for the host')
	args = par.parse_args()
	#print(minsteps)
	#installonshutdown = false_unless_true_lowercase(args.installonshutdown)
	hname = args.hname.strip()
	halias = args.halias.strip()
	haddress = args.haddress.strip()

	objects_mn = OBJECTS_MN_TEMPLATE.format(hname,
						halias,haddress)
	idx = 0
	for idx,line in enumerate(objects_mn.splitlines()):
		print(line)
	if idx > 0:
		print('#')
		dtiso = dt.now().isoformat()
		print("# Generated {0} lines at {1}".format(idx,dtiso))
	exit(0)

