#!/bin/bash
# This is a sample shell script showing how you can submit the SCHEDULE_FORCED_SVC_CHECK command
# to Nagios.  Adjust variables to fit your environment as necessary.
# Debian has /nagios/rw/ directory at /var/lib/nagios3/rw/
#
# dpkg-statoverride --update --add nagios nagios 751 /var/lib/nagios3
#
# Web control panel complains about permissions? Your responsibility this change but here it is:
## dpkg-statoverride --update --add nagios www-data 2730 /var/lib/nagios3/rw
# Tighter but no web: dpkg-statoverride --update --add nagios nagios 2730 /var/lib/nagios3/rw
#
# apt-get --no-install-recommends install nagios3-common nagios3-core nagios3-cgi nagios-images
# 
# htpasswd -b /etc/nagios3/htpasswd.users nagiosadmin "$admpass"
# ...or...
# dpkg-reconfigure nagios3-cgi
#
# Which package creates /var/lib/nagios3/rw/nagios.cmd ?
# Answer: It is not created at install time but is a feature of the following directives in nagios.cfg
# check_external_commands=1
# command_check_interval=10s
# command_file=/var/lib/nagios3/rw/nagios.cmd
#
# Recipe example for the nagios package installs? nagios34.sls

/usr/bin/getent group nagios

secs_now=`date +%s`
secs_now1=`date +%s -d "+1 minute"`

#commandfile='/usr/local/nagios/var/rw/nagios.cmd'
commandfile='/var/lib/nagios3/rw/nagios.cmd'
#dpkg-statoverride --update --add nagios nagios 2730 /var/lib/nagios3/rw
#
# If you do 'touch $commandfile' without having nagios itself do the creation
# then you are probably doing things WRONG
#
chgrp nagios $commandfile
chmod g+w $commandfile
vdir -d /var/lib/nagios3/
vdir -d /var/lib/nagios3/rw
vdir $commandfile
# cmd4 is the 4 fields concatenated (excludes the seconds prefix)
# printf will fail if you include trailing spaces on the field so avoid doing '/var ;'
printf -v cmd4 'SCHEDULE_FORCED_SVC_CHECK;localhost;DiskSpaceFSvar;%lu' $secs_now1
# Assigned to var above using a bashism (-v)
printf '[%lu] %s\n' $secs_now $cmd4
# Printed it (for preview above), next send it to commmandfile
#printf $cmd1 > $commandfile
printf '[%lu] %s\n' $secs_now $cmd4 > $commandfile

