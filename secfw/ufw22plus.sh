#!/bin/sh

ens_print () {
  for item in $(netstat -i | awk '!/Kernel|Iface|lo/ {print $1," "}')
  do
    printf "%s\n" "$item"
  done

}

firewall_allow_limit () {
  printf "Installing and setting up firewall to allow incoming on port 80\n"
  ufw allow 80/tcp comment "Http port (80)"
  #ufw allow $RPCPORT/tcp comment "$CRYPTONAME rpc port" >/dev/null
  ufw allow ssh comment "Ssh server"
  ufw limit ssh/tcp
  ufw default allow outgoing >/dev/null 2>&1
  printf "y\n" | ufw enable
}

# Call functions. Additional print before interfaces.
firewall_allow_limit
printf "Following interfaces probably affected now by your new firewall\n"
ens_print

