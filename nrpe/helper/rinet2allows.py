#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2012-2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from sys import argv,exit
import socket
from datetime import datetime as dt

# Example run to stdout
# python ./rinet2allows.py 4.4.4.4 1666 127.0.0.1 5666 nextallows 1.1.1.1 2.2.2.2
#
# Example run to populate /etc/nagios/nrpe.d/10allowed_hosts4.cfg
# ./rinet2allows.py 4.4.4.4 1666 127.0.0.1 5666 nextallows 1.1.1.1 2.2.2.2 > /etc/rinetd.conf
#
# The string nextallows is ignored and becomes a comment rather than an allow line

TWO_TO_SIXTEEN = 2**16
# chr(44) is comma, chr(58) is colon (:)

HEADER_TAGLINE='the internet redirection server'

RINETCONF_TEMPLATE1 = """#
# you may specify global allow and deny rules here
# only ip addresses are matched, hostnames cannot be specified here
# the wildcards you may use are * and ?
#
# allow 192.168.2.*
# deny 192.168.2.1?


#
# forwarding rules come here
#
# you may specify allow and deny rules after a specific forwarding rule
# to apply to only that forwarding rule
#
# bindadress    bindport  connectaddress  connectport
"""

""" Above and next are templates that could in future allow substitutions
but for now are just plain text with no placeholders
"""

RINETCONF_TEMPLATE2 = """

# logging information
logfile /var/log/rinetd.log

# uncomment the following line if you want web-server style logfile format
# logcommon
"""


def ip4valid(ipv4string):
	""" Returns stripped ip4 if a valid IP version 4 address
	otherwise returns None
	"""	
	addr_return = None
	addr_stripped = ipv4string.strip()
	try:
		socket.inet_aton(addr_stripped)
		addr_return = addr_stripped
	except:
		pass
	return addr_return


def validport(port_given,port_minimum=1024):
	port_int = None
	try:
		port_int = int(port_given)
	except:
		pass
	if port_int >= TWO_TO_SIXTEEN:
		# Above maximum allowed value for port
		return None
	elif port_int < 1:
		# Too small
		return None
	elif port_int < port_minimum:
		# Below limit based on supplied arg/defaul
		return None
	else:
		# Valid port passing all limit tests
		pass

	return port_int


if __name__ == '__main__':

	program_binary = argv[0].strip()
	# Args 1 -> 4 probably something like 4.4.4.4 60666 127.0.0.1 5666
	allow_list = argv[5:]
	addr4list = []
	addr_rejected_list = []

	# In the next loop we do slightly more than ip4valid() so use in loop code
	addr4count = 0
	for addr in allow_list:
		addr_stripped = addr.strip()
		try:
			socket.inet_aton(addr_stripped)
			addr4count += 1
			addr4list.append(addr_stripped)
		except:
			addr_rejected_list.append(addr_stripped)

	dtiso = dt.isoformat(dt.now())

	print("# Config file for rinetd, {0} (generated {1}).".format(HEADER_TAGLINE,dtiso))

	if len(addr_rejected_list) > 0:
		for addr in addr_rejected_list:
			print("# Rejected allow arg:{0}".format(addr))

	print(RINETCONF_TEMPLATE1)
	""" Line to generate next look something like below:
	aa.bb.cc.dd  1666        127.0.0.1  5666
	"""
	ip1 = ip4valid(argv[1])
	if ip1 is None:
		exit(151)
	port1 = validport(argv[2])
	if port1 is None:
		exit(152)
	ip_after_translate = ip4valid(argv[3])
	if ip_after_translate is None:
		exit(153)
	port2 = validport(argv[4])
	if port2 is None:
		exit(154)

	print("{0}  {1}        {2}  {3}".format(ip1,port1,ip_after_translate,port2))
	for addr in addr4list:
		print("allow {0}".format(addr))
	print("deny all")
	print(RINETCONF_TEMPLATE2)

