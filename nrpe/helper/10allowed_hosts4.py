#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2012-2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from sys import argv
import socket
from datetime import datetime as dt

# chr(44) is comma, chr(58) is colon (:)

# Example run to stdout
# python ./10allowed_hosts4.py 1.1.1.1 2.2.2.2 badarg1
#
# Example run to populate /etc/nagios/nrpe.d/10allowed_hosts4.cfg
# ./10allowed_hosts4.py 1.1.1.1 2.2.2.2 badarg1 > /etc/nagios/nrpe.d/10allowed_hosts4.cfg

if __name__ == '__main__':

	program_binary = argv[0].strip()
	address_list = argv[1:]
	addr4list = []
	addr_rejected_list = []

	addr4count = 0
	for addr in address_list:
		addr_stripped = addr.strip()
		try:
			socket.inet_aton(addr_stripped)
			addr4count += 1
			addr4list.append(addr_stripped)
		except:
			addr_rejected_list.append(addr_stripped)

	dtiso = dt.isoformat(dt.now())
	print("# {0} Define locally configured allowed_hosts".format(dtiso))
	print("# Best guess at my file location is /etc/nagios/nrpe.d/10allowed_hosts4.cfg")

	if len(addr_rejected_list) > 0:
		for addr in addr_rejected_list:
			print("# Rejected allow arg:{0}".format(addr))

	allowed_csv = chr(44).join(addr4list)
	print("allowed_hosts={0}".format(allowed_csv))

