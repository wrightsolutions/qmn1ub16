sudo_cmdal_local:
  file.managed:
  - name: /etc/sudoers.d/sudoCmnd_Alias
  - contents: |
      # Cmnd alias specification
      Cmnd_Alias      SHUTDOWN = /usr/sbin/shutdown
      Cmnd_Alias      POWERMGMT = /usr/bin/cpufreq-selector
      Cmnd_Alias      DUMPS = /usr/bin/mt, /usr/sbin/dump, /usr/sbin/rdump, /usr/sbin/restore, /usr/sbin/rrestore
      Cmnd_Alias      KILL = /usr/bin/pkill, /bin/kill, /usr/bin/kill
      Cmnd_Alias      PRINTING = /usr/sbin/lpc, /usr/bin/lprm
      Cmnd_Alias      HALT = /usr/sbin/halt, /usr/sbin/fasthalt
      Cmnd_Alias      REBOOT = /usr/sbin/reboot, /usr/sbin/fastboot
      Cmnd_Alias      SHELLS = /bin/dash, /bin/bash, /usr/bin/sh, /usr/bin/csh, /usr/bin/ksh
      Cmnd_Alias      SU = /bin/su, /bin/su -l, /usr/bin/su, /usr/bin/su -l
      Cmnd_Alias      MONLOAD = /usr/bin/uptime, /usr/bin/downtimes, /usr/bin/top -s, /usr/bin/htop
      Cmnd_Alias      KILLALL = /usr/bin/pkill, /bin/kill, /usr/bin/killall, /bin/killall
      Cmnd_Alias      MONNETSEC = /bin/netstat, /bin/netstat -anp, /usr/bin/lsof -i -n -P, /usr/sbin/nethogs
      Cmnd_Alias      MONPROC = /bin/ps axuwwwf, /bin/ps -ef, /usr/bin/top s n1, /usr/bin/top s n1 b
      Cmnd_Alias      MONFWALL = /usr/sbin/iptables --list-rules, /usr/sbin/iptables -t filter --list-rules, /usr/lib64/nagios/plugins/check_iptables_input.sh *, /usr/sbin/iptables -t nat --list-rules
      Cmnd_Alias      CAPACITY = /bin/df, /usr/bin/du, /usr/bin/du -sh *
      Cmnd_Alias      RCOPY = /usr/bin/scp -Bq *
      Cmnd_Alias      WHOM = /usr/bin/w, /usr/bin/last, /usr/bin/last -ai
      Cmnd_Alias      DEBCONF = /usr/bin/debconf-show
      Cmnd_Alias      NAGIOSPLUGINS = /usr/lib/nagios/plugins/check_*, /usr/lib64/nagios/plugins/check_*
      Cmnd_Alias      VDIR = /bin/vdir -dlp *, /bin/vdir -p /var/log/*
      Cmnd_Alias      SSSDCACHE = /usr/sbin/sss_cache -E, /usr/sbin/sss_cache -G, /usr/sbin/sss_cache -U, /usr/sbin/sss_usermod -a * , /usr/sbin/sss_cache --group , /usr/sbin/sss_cache --user
      Cmnd_Alias      SSSDCTL = /sbin/sssctl, /usr/sbin/sss_groupshow
      Cmnd_Alias      OLBIN = /opt/local/bin/*.sh, /opt/local/bin/*.py
      Cmnd_Alias      OLBINOP = /opt/local/binop/*.sh, /opt/local/binop/*.py
      Cmnd_Alias      BINOPVARDSH = /bin/df -m, /usr/bin/du -sh /var/log/*

  - mode: 640
  - backup: minion

