#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2012-2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

#import argparse
from os import getenv as osgetenv
from sys import exit,argv,stdin
#import fileinput
import re
from string import printable
#from string import ascii_letters,digits,printable

""" Expects /etc/hosts to be piped in and one or more
lines of that hosts file to be of 127.0. style lines

Because /etc/hosts is an important system file we automate
the alias adding via script to prevent errors in manual editing.

Suggest running first time with PYVERBOSITY=3 as follows
export PYVERBOSITY=3

cat /etc/hosts | ./hosts4aliasadder.py 'superserver.somedomain.com superserver'
# Above example adds two aliases (one fqdn and one short) to the most appropriate 127.0.?.? line in
# the /etc/hosts input given to the script.

Try this next example to ensure even if you give reversed input, the alias still gets appended to good line
sort -r /etc/hosts | ./hosts4aliasadder.py 'serveralias1' 'serveralias2'
"""

verbosity_global = 1
# Above is set by default to 1. Also see __name__ == '__main__' section later.

SET_PRINTABLE=set(printable)
TRANSLATE_DELETE = ''.join(map(chr, range(0,9)))
#print(len(TRANSLATE_DELETE))

TABBY=chr(9)

WARN_PREFIX='WARNING: Non-printing characters detected'

re_127zero = re.compile('127\.0\.[0-9]+\.[0-9]+\s+[a-zA-Z0-9]+')
re_commenthashy = re.compile('\s+#')

candidate_dict = {}


def add_aliases(dict_of_lines,aliases_extra,verbosity=0):
	"""
	Could instead rework this script for OrderedDict instead of the lambda but less compatible.
	We return a dict with index of line to be replaced and new line to replace it with
	"""
	len_input = len(dict_of_lines)
	lines = ['# placeholder from script']*len_input
	#print(lines)
	# Above is a way of ensuring standard text in place incase loop assignments not completed
	dict_sorted_on_val_reverse = sorted(dict_of_lines.items(), key=lambda v: v[1], reverse=True)
	if verbosity > 2:
		print('# Next we show dictionary of candidates supplied to add_aliases() [reversed]')
		print(dict_sorted_on_val_reverse)

	replacement_dict = {}
	# Working last line first we continue until we find first 127.0. style
	line_new = ''
	idx = 0
	for dkey,val in dict_sorted_on_val_reverse:
		if re_127zero.match(val) and len(line_new) < 2:
			# line_new having postive length acts as stop on entering here twice
			line_new = "{0}{1}{1}{2}".format(val.rstrip(),TABBY,TABBY.join(aliases_extra))

			try:
				replacement_dict[dkey] = line_new
				lines[idx] = line_new
			except IndexError:
				print("# List index problem for index={0} key={1}".format(idx,dkey))
			if verbosity > 0:
				print("Candidate {0} now: {1}".format(idx,line_new))
		else:
			lines[idx] = val.rstrip()
		idx+=1

	if verbosity > 2:
		print(lines)

	return replacement_dict


def process_lines(lines,verbosity=1):
	""" Process the lines and deal with non-printables in one of two ways:
	(1) when verbosity > 0 we report as a comment and have printable lines make up list lines_f
        (2) when verbosity is 0 we quit processing when see non-printable and clear lines_f

	We add 127.0 style lines that are not commented, to global candidate_lines
	We return lines_f so any post processing relevant to 'valid lines' can use returned list
	"""
	global candidate_dict
	lines_f = []     # Lines filtered - initially empty list
	for idx,line in enumerate(lines):
		if not set(line).issubset(SET_PRINTABLE):
			if verbosity > 0:
				print("# {0} in line {1} of input /etc/hosts".format(WARN_PREFIX,idx))
				continue
			else:
				lines_f = []
				break
		lines_f.append(line.rstrip())
		# All lines that reach this far have been added to lines_f
                # and are free of non-printables
		if re_commenthashy.search(line):
			pass
		elif re_127zero.match(line):
			candidate_dict[idx] = line
		else:
			pass
	return lines_f


if __name__ == '__main__':

	program_binary = argv[0].strip()

	PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
	PYVERBOSITY = 1
	if PYVERBOSITY_STRING is None:
		pass
	else:
		try:
			PYVERBOSITY = int(PYVERBOSITY_STRING)
		except:
			pass
	#print(PYVERBOSITY)

	""" Next verbosity_global = 1 is the most likely outcome
	unless the user specifically overrides it by setting
	the environment variable PYVERBOSITY
	"""
	if PYVERBOSITY is None:
		verbosity_global = 1
	elif 0 == PYVERBOSITY:
		# 0 from env means 0 at global level
		verbosity_global = 0
	elif PYVERBOSITY > 1:
		# >1 from env means same at global level
		verbosity_global = PYVERBOSITY
	else:
		verbosity_global = 1


	lines_printable = process_lines(stdin.readlines(),1)
	len_lines = len(lines_printable)
	if len_lines < 2:
		# Lines of /etc/hosts either not typical two lines or more style or have non-printables
		exit(110)

	candidate_count = len(candidate_dict)
	if candidate_count < 2:
		# Could not find two or more candidate lines that did not contain non-printables
		exit(112)


	if verbosity_global < 2:
		rdict = add_aliases(candidate_dict,argv[1:])
	else:
		rdict = add_aliases(candidate_dict,argv[1:],verbosity_global)

	""" To make the the actual replacement we take the info in rdict and apply it
	to candidate_dict
	"""
	rdict_keys = rdict.keys()
	idx_replace = rdict_keys[0]
	"""
	for idx,val in candidate_dict.items():
		if idx == idx_replace:
			print(rdict[idx_replace])
		else:
			print(val.rstrip())
	"""

	for idx,line in enumerate(lines_printable):
		if idx == idx_replace:
			if line == candidate_dict[idx_replace].rstrip():
				print(rdict[idx_replace].rstrip())
			else:
				print('# Error during post-processing ... quitting')
				print(line,candidate_dict[idx_replace])
				break
		else:
			print(line.rstrip())

	exit(0)

