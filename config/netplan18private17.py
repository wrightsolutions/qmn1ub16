#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from sys import argv,exit
import socket
from datetime import datetime as dt

""" Generate yaml for netplan networking configuration helper based on 6
items of input:
public address
private address (will be suffixed /17)
dns server 1
dns server 2

Tested against netplan 0.40.1. Needs testing with newer 0.95-2

With generated yaml in appropriate /etc/netplan/ you can issue 'netplan apply'
"""

# Example run to stdout
# python ./netplan18private17.py 178.100.41.88 192.168.100.4 2400:8901:: 178.100.41.1 8.8.8.8 8.8.4.4


NETPLAN17TEMPLATE="""# This file describes the network interfaces available on your system
# For more information, see netplan(5).
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: no
      dhcp6: no
      addresses:
        - {main4}/24                                    # IPv4 address - public
        - {private4}/17                                 # Private IPv4 address
        - "{main6}/64"                                  # IPv6 address - public
      gateway4: {gateway4}                              # Gateway IPv4 (primary)
      gateway6: "fe80::1"                                   # Gateway IPv6 (primary)
      nameservers:
        search: [members.linode.com]                        # Search domain for dns
        addresses: [{dns1},{dns2}]                  # DNS Server IP addresses
{footer1generated} 
"""


def ip4valid(ipv4string,level=1,verbosity=0):
	""" Returns stripped ip4 if a valid IP version 4 address
	otherwise returns None
	Set level=1 if you want a more rigorous check (via network)
	"""
	addr_return = None
	addr_stripped = ipv4string.strip()
	if level > 0:
		try:
			socket.inet_aton(addr_stripped)
			addr_return = addr_stripped
		except:
			if verbosity > 0:
				print("ip4valid() except triggered for level=1 check")
			pass
	else:
		# Just do rudimentary checking of looks like ip address
		try:
			octet_array = addr_stripped.split('.')
			#print(octet_array)
			if len(octet_array) == 4:
				addr_list = [int(b) for b in octet_array]
				addr_list = [b for b in addr_list if b >= 0 and b<=255]
			if len(addr_list) == 4:
				addr_return = addr_stripped
		except:
			if verbosity > 0:
				print("ip4valid({0}) except triggered for level=0 check".format(addr_stripped))
			pass
	return addr_return


def validport(port_given,port_minimum=1024):
	port_int = None
	try:
		port_int = int(port_given)
	except:
		pass
	if port_int >= TWO_TO_SIXTEEN:
		# Above maximum allowed value for port
		return None
	elif port_int < 1:
		# Too small
		return None
	elif port_int < port_minimum:
		# Below limit based on supplied arg/defaul
		return None
	else:
		# Valid port passing all limit tests
		pass

	return port_int


if __name__ == '__main__':

	program_binary = argv[0].strip()
	# Args 1 -> 6 probably something like 178.100.41.88 ...
	main4 = ip4valid(argv[1],0)
	if main4 is None:
		exit(151)
	private4 = ip4valid(argv[2],0,1)
	if private4 is None:
		exit(152)
	# First two arguments are stored. Next we will store main6
	main6 = argv[3]
	#
	gateway4 = ip4valid(argv[4],0)
	if gateway4 is None:
		exit(154)
	#
	dns1 = ip4valid(argv[5])
	if dns1 is None:
		exit(155)
	dns2 = ip4valid(argv[6])
	if dns2 is None:
		exit(156)

	# In the next loop we do slightly more than ip4valid() so use in loop code
	addr4count = 0
	addr_rejected_list = []
	dns4list = []
	for dns4 in [dns1,dns2]:
		addr_stripped = dns4.strip()
		try:
			socket.inet_aton(addr_stripped)
			addr4count += 1
			dns4list.append(addr_stripped)
		except:
			addr_rejected_list.append(addr_stripped)
	if len(dns4list) < 2:
		exit(159)

	dtiso = dt.isoformat(dt.now())

	footer1generated = "# Yaml config for netplan - generated {0}.".format(dtiso)

	if len(addr_rejected_list) > 0:
		pass

	dict_args = {'main4': main4,
		'private4': private4,
		'main6': main6,
		'gateway4': gateway4,
		'dns1': dns1,
		'dns2': dns2,
		'footer1generated': footer1generated,
	}

	netplan_out = NETPLAN17TEMPLATE.format(**dict_args)
	for line in netplan_out.splitlines():
		print(line)

