sysctl_vm_local:
  file.managed:
  - name: /etc/sysctl.d/90-vm_local.conf
  - contents: |
      # Local or custom settings for vm.
      #
      vm.swappiness = 10
      # swappiness is changed from typical 60 to lower value
      
      vm.vfs_cache_pressure = 50
      # vfs_cache_pressure is changed from typical 100 to lower value
      
  - backup: minion


sysctl_load_reload_target90:
  cmd.run:
  - name: /sbin/sysctl -p /etc/sysctl.d/90-vm_local.conf
  - require:
    - file: sysctl_vm_local
  - order: last


#sysctl_load_reload_all:
#  cmd.run:
#  - name: /sbin/sysctl -p
#  - require:
#    - file: sysctl_vm_local
#  - order: last

