#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

import argparse
from datetime import datetime as dt
#from os import getenv as osgetenv
from sys import argv,exit

""" Set 6 flags in Apt conf.d/unattended-upgrades based on args.
In particular the planned supported examples with 0,false equivalent and 1,true,yes equivalent
./apt_unattended.py --autofixinter false --minsteps true --installonshutdown false --autoreboot false
./apt_unattended.py --autoreboot false
./apt_unattended.py --autofixinter true --mailonlyonerror false --autoreboot false
./apt_unattended.py --autofixinter 0 --mailonlyonerror 1 --autoreboot false
./apt_unattended.py --autofixinter 0 --mailonlyonerror yes --autoreboot no
# autoreboothour not in initial implementation
./apt_unattended.py --minsteps true --autoreboot false --autoreboothour 7
./apt_unattended.py --autofixinter true --mailonlyonerror false --autoreboothour 7
./apt_unattended.py --autoreboot 0 --autoreboothour 7
defaults:	true, true, true, false, false, false, 2

Prints to stdout. Might want to redirect (>) to /etc/apt/apt.conf.d/50unattended-upgrades
mn preferred:  --autofixinter false --minsteps true --installonshutdown false --autoreboot false
"""

DQ=chr(34)

verbosity_global = 1
# Above is set by default to 1. Also see __name__ == '__main__' section later.

ADESC="""Arguments for setting values in Apt conf.d/unattended-upgrades."""

# Below we have replaced ${distro_id} so treated as literal { rather than template place
APT_CONFD_UNATT_TEMPLATE="""
// Automatically upgrade packages from these (origin:archive) pairs
Unattended-Upgrade::Allowed-Origins {{
	"${{distro_id}}:${{distro_codename}}";
	"${{distro_id}}:${{distro_codename}}-security";
	// Extended Security Maintenance; doesn't necessarily exist for
	// every release and this system may not have it installed, but if
	// available, the policy for updates is such that unattended-upgrades
	// should also install from here by default.
	"${{distro_id}}ESM:${{distro_codename}}";
//	"${{distro_id}}:${{distro_codename}}-updates";
//	"${{distro_id}}:${{distro_codename}}-proposed";
//	"${{distro_id}}:${{distro_codename}}-backports";
}};

// List of packages to not update (regexp are supported)
Unattended-Upgrade::Package-Blacklist {{
//	"vim";
//	"libc6";
//	"libc6-dev";
//	"libc6-i686";
	"cloud-init";
	"grub-pc";
	"grub-pc-bin";
	"grub2-common";
	"kmod";
	"libkmod2";
}};

// This option allows you to control if on a unclean dpkg exit
// unattended-upgrades will automatically run 
//   dpkg --force-confold --configure -a
// The default is true, to ensure updates keep getting installed
Unattended-Upgrade::AutoFixInterruptedDpkg {0}{1}{0};

// Split the upgrade into the smallest possible chunks so that
// they can be interrupted with SIGUSR1. This makes the upgrade
// a bit slower but it has the benefit that shutdown while a upgrade
// is running is possible (with a small delay)
Unattended-Upgrade::MinimalSteps {0}{2}{0};

// Install all unattended-upgrades when the machine is shuting down
// instead of doing it in the background while the machine is running
// This will (obviously) make shutdown slower
Unattended-Upgrade::InstallOnShutdown {0}{3}{0};

// Send email to this address for problems or packages upgrades
// If empty or unset then no email is sent, make sure that you
// have a working mail setup on your system. A package that provides
// 'mailx' must be installed. E.g. "user@example.com"
Unattended-Upgrade::Mail "root";

// Set this value to "true" to get emails only on errors. Default
// is to always send a mail if Unattended-Upgrade::Mail is set
Unattended-Upgrade::MailOnlyOnError {0}{4}{0};

// Do automatic removal of new unused dependencies after the upgrade
// (equivalent to apt-get autoremove)
Unattended-Upgrade::Remove-Unused-Dependencies {0}{5}{0};

// Automatically reboot *WITHOUT CONFIRMATION*
//  if the file /var/run/reboot-required is found after the upgrade 
Unattended-Upgrade::Automatic-Reboot {0}{6}{0};

// If automatic reboot is enabled and needed, reboot at the specific
// time instead of immediately
//  Default: "now"
//Unattended-Upgrade::Automatic-Reboot-Time "02:00";

// Use apt bandwidth limit feature, this example limits the download
// speed to 70kb/sec
//Acquire::http::Dl-Limit "70";
"""


def false_unless_true(bool_str):
	""" Process an arg into lowercase true or false 
	based on appropriate interpretation of the string.
	Although argument is labelled 'bool_str' we accept and
	deal with other cases typical of a non validated input
	In argparse might want type=false_unless_true (untested)
	"""
	bool_return = False
        # Do keep False as the default or trace conditions carefully before changing
	try:
		blower = bool_str.strip().lower()
	except:
		# Not a string
		if bool_str is True:
			bool_return = True
		else:
			# False or some other non-string value
			pass
			#print(bool_str,bool_str)
	try:
		blower_int = int(blower)
		if 1 == blower_int:
			bool_return = True
	except:
		if bool_return is False:
			if bool_str is False:
				pass
			else:
				try:
					if blower in ('true', 't', 'yes'):
						bool_return = True
				except:
					EMSG_PREFIX = 'Unexpected value supplied: '
					raise Exception(EMSG_PREFIX+bool_str)

	return bool_return


def false_unless_true_lowercase(bool_str):
	""" Wrapper around false_unless_true where we REALLY want
	a response that is a stringified lowercase true or false
	"""
	bool_tf = false_unless_true(bool_str)
	bool_lower = 'true'
	if bool_tf is False:
		bool_lower = 'false'
	return bool_lower


if __name__ == '__main__':

	program_binary = argv[0].strip()

	par = argparse.ArgumentParser(description=ADESC)
	# Avoid type=bool or similar as that kind of processing is after args assigned here.
	par.add_argument('--autofixinter', default=True,
				dest='autofixinter',
				help='Auto Fix Interrupted Dpkg')
	par.add_argument('--minsteps', default=True,
				dest='minsteps',
				help='Minimal Steps')
	par.add_argument('--installonshutdown', default=True,
				dest='installonshutdown',
				help='Install On Shutdown')
	par.add_argument('--mailonlyonerror', default=False,
				dest='mailonlyonerror',
				help='Mail Only On Error')
	par.add_argument('--removeunuseddep', default=False,
				dest='removeunuseddep',
				help='Remove Unused Dependencies')
	par.add_argument('--autoreboot', default=False,
				dest='autoreboot',
				help='Automatic Reboot')
	args = par.parse_args()
	autofixinter = false_unless_true_lowercase(args.autofixinter)
	#print(autofixinter)
	minsteps = false_unless_true_lowercase(args.minsteps)
	#print(minsteps)
	installonshutdown = false_unless_true_lowercase(args.installonshutdown)
	mailonlyonerror = false_unless_true_lowercase(args.mailonlyonerror)
	removeunuseddep = false_unless_true_lowercase(args.removeunuseddep)
	autoreboot = false_unless_true_lowercase(args.autoreboot)

	apt_confd_unatt = APT_CONFD_UNATT_TEMPLATE.format(DQ,
					autofixinter,
					minsteps,
					installonshutdown,
					mailonlyonerror,
					removeunuseddep,
					autoreboot)
	idx = 0
	for idx,line in enumerate(apt_confd_unatt.splitlines()):
		print(line)
	if idx > 0:
		print('//')
		dtiso = dt.now().isoformat()
		print("// Generated {0} lines at {1}".format(idx,dtiso))
	exit(0)

